CREATE DATABASE rtdb;
GO
CREATE login rtdbadmin WITH PASSWORD='123123', CHECK_POLICY=OFF, DEFAULT_DATABASE=rtdb;
GO
USE rtdb;
GO
CREATE USER rtdbadmin FOR LOGIN rtdbadmin;
exec sp_addrolemember 'db_owner', 'rtdbadmin';
GO