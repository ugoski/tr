#!/bin/sh
#Asking user for password
echo "Please remember the following database password."
echo "It will be required during the installation wizard."
read -s -p "Please enter a strong database password: " DEFAULT_PASSWORD

echo ""
#Create TestRail directories
echo "Creating TestRail Directories now."
sudo mkdir /opt/testrail
sudo mkdir /opt/testrail/logs
sudo mkdir /opt/testrail/attachments
sudo mkdir /opt/testrail/reports
sudo mkdir /opt/testrail/audit

sudo chown www-data:www-data /opt/testrail/logs
sudo chown www-data:www-data /opt/testrail/attachments
sudo chown www-data:www-data /opt/testrail/reports
sudo chown www-data:www-data /opt/testrail/audit

echo "Directories have been created!"

sudo apt-get update -y;

#Installing Apache, PHP, MySQL and pre-requisites.
sudo apt-get install unzip apache2 php7.2 libapache2-mod-php7.2 php7.2-mysql php7.2-curl php7.2-mbstring php7.2-xml php7.2-zip mysql-server mysql-client -y;
echo "Apache, PHP 7.2, PHP packages are now installed! Resuming now."
sudo echo "<?php phpinfo(); ?>" > /var/www/html/phpinfo.php ;
sudo wget https://downloads.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.zip ;
sudo unzip ioncube_loaders_lin_x86-64.zip ;
sudo cp ioncube/ioncube_loader_lin_7.2.so /usr/lib/php/20170718 ;
sudo echo "zend_extension=ioncube_loader_lin_7.2.so" >> /etc/php/7.2/apache2/php.ini ;
sudo systemctl restart apache2 ;
sudo systemctl restart mysql;

#SQL commands to be referenced later
SQL_COMMAND_1="CREATE DATABASE testrail DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;"
SQL_COMMAND_2="CREATE USER 'testrail'@'localhost' IDENTIFIED BY '${DEFAULT_PASSWORD}';"
SQL_COMMAND_3="GRANT ALL ON testrail.* TO 'testrail'@'localhost';"

#Create TestRail Database and User
echo "Setting up TestRail Database"
mysql -u root << eof
$SQL_COMMAND_1
eof

mysql -u root << eof
$SQL_COMMAND_2
eof

mysql -u root << eof
$SQL_COMMAND_3
eof

echo "Database has been configured."

echo "Running MySQL Secure installation now!"
sleep 4s
mysql_secure_installation